import * as fs from "fs";
import * as util from "util";
import * as xml2js from "xml2js";

import { DiagnosisOccurrences, Diagnoses, DiagnosisSymptomRule, Symptoms } from './types';

export async function loadOccurrences(): Promise<DiagnosisOccurrences> {
    const f = await util.promisify(fs.readFile)("data/Diagnoses/DiagnosisOccurrences.xml");
    const o = await xml2js.parseStringPromise(f.toString());
    const occurrences = o["Database"]["GameDBDiagnosisOccurrence"];

    let result: DiagnosisOccurrences = new Map();

    for (let i = 0; i < occurrences.length; i++) {
        let id = occurrences[i]["$"]["ID"];
        let occurrenceRate = parseInt(occurrences[i]["OccurrenceRate"][0]);

        result.set(id, { occurrenceRate });
    }

    return result;
}

export async function loadDiagnoses(filename: string): Promise<Diagnoses> {
    const f = await util.promisify(fs.readFile)(`data/Diagnoses/${filename}`);
    const o = await xml2js.parseStringPromise(f.toString());
    const conditionsObj = o["Database"]["GameDBMedicalCondition"];

    let result: Diagnoses = new Map();

    for (let i = 0; i < conditionsObj.length; i++) {
        let id = conditionsObj[i]["$"]["ID"];
        let duration = parseFloat(conditionsObj[i]["Duration"][0]);
        let occurrenceId = conditionsObj[i]["OccurrenceRef"][0];
        let departmentId = conditionsObj[i]["DepartmentRef"][0];
        let symptomsArray = conditionsObj[i]["Symptoms"][0]["GameDBSymptomRules"];

        let symptomRules: DiagnosisSymptomRule[] = [];

        for (let symIdx = 0; symIdx < symptomsArray.length; symIdx++) {
            let symptom = symptomsArray[symIdx];
            let symptomRef = symptom["GameDBSymptomRef"][0];
            let dayOfFirstOccurrence = 0;
            let probability = 0;

            if (symptom["DayOfFirstOccurence"]) { // sic
                dayOfFirstOccurrence = parseFloat(symptom["DayOfFirstOccurence"][0]);
            } else {
                // console.warn(`${id}: day of first occurrence missing, 0 assumed`);
            }

            if (symptom["ProbabilityPercent"]) {
                probability = parseFloat(symptom["ProbabilityPercent"][0]) / 100;
            } else {
                console.warn(`${id}: probability missing for symptom ${symptomRef}, 0 assumed`);
            }

            symptomRules.push({ dayOfFirstOccurrence, probability,
                symptomRef });
        }

        result.set(id, {
            id, duration, occurrenceId, departmentId, symptomRules: symptomRules
        });
    }

    return result;
}

export async function loadSymptoms(filename: string): Promise<Symptoms> {
    const f = await util.promisify(fs.readFile)(`data/${filename}`);
    const o = await xml2js.parseStringPromise(f.toString());
    const symptomsObj = o["Database"]["GameDBSymptom"];

    let result: Symptoms = new Map();

    for (let i = 0; i < symptomsObj.length; i++) {
        let id = symptomsObj[i]["$"]["ID"];
        let patientComplains = 
            symptomsObj[i]["PatientComplains"][0].toLowerCase() === "true";
        let isMainSymptom =
            symptomsObj[i]["IsMainSymptom"][0].toLowerCase() === "true";
        let shameLevel = parseFloat(symptomsObj[i]["ShameLevel"][0]) / 100;

        let examinationIds = symptomsObj[i]["Examinations"] ? 
            symptomsObj[i]["Examinations"][0]["ExaminationRef"] : [];
        let treatmentIds = symptomsObj[i]["Treatments"] ?
            symptomsObj[i]["Treatments"][0]["TreatmentRef"] : [];

        let symptom = { id, patientComplains, examinationIds, treatmentIds,
            isMainSymptom, shameLevel };
        result.set(id, symptom);
    }

    return result;
}
