import * as fs from "fs";
import * as readline from "readline";
import * as util from "util";

import { CaseDbEntry } from "./types";
import { DecisionTree } from "./decision-tree";

async function main() {
    let rl = readline.createInterface(fs.createReadStream("case-db.json"));

    // load db from disk
    let caseDb: CaseDbEntry[] = [];

    for await (const line of rl) {
        let caseObj = JSON.parse(line);
        caseDb.push(caseObj);
    }

    // train model
    let tree = new DecisionTree();
    
    tree.train(caseDb);

    // store model
    let modelFile = await util.promisify(fs.open)("model.json", "w+");

    await util.promisify(fs.write)(modelFile, tree.save());

    await util.promisify(fs.close);
}

main();