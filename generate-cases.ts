import * as fs from "fs";
import * as util from "util";

import { loadOccurrences, loadDiagnoses, loadSymptoms } from './loader';
import { Diagnoses, Symptoms, SymptomState, SymptomPresentation, Case, CaseDbEntry }
    from './types';

declare global {
    interface Map<K, V> {
        merge(other: Map<K, V>): void;
    }    
}

Map.prototype.merge = function<K, V> (other: Map<K, V>) {
    for (let kvp of other) {
        this.set(kvp[0], kvp[1]);
    }
}

// q.v. UnityEngine.Random.Range(int, int):
// https://docs.unity3d.com/ScriptReference/Random.Range.html
function randomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) + min);
}

// q.v. PerkSet.DEMO_CreatePatientPerkSet and
// BehaviorPatient.GetShameLevelMultiplier, with all the irrelevant bits removed
function generatePatientShameModifier(): number {
    let nPerks = 0, perkCount = randomInt(1, 3);
    let shameModifier = 1;

    if (randomInt(0, 100) < 5 && nPerks < perkCount) nPerks++; // pirate
    if (randomInt(0, 100) < 12 && nPerks < perkCount) nPerks++; // germophobe
    if (randomInt(0, 100) < 12 && nPerks < perkCount) nPerks++; // medical education
    if (randomInt(0, 100) < 18 && nPerks < perkCount) nPerks++; // foreigner
    if (randomInt(0, 100) < 30 && nPerks < perkCount) nPerks++; // fast metabolism
    if (randomInt(0, 100) < 30 && nPerks < perkCount) nPerks++; // gamer
    if (randomInt(0, 100) < 30 && nPerks < perkCount) nPerks++; // clean/dirty feet
    if (randomInt(0, 100) < 18 && nPerks < perkCount) nPerks++; // cooperative/aggressive
    if (randomInt(0, 100) < 18 && nPerks < perkCount) nPerks++; // inspirational/demotivator
    if (randomInt(0, 100) < 30 && nPerks < perkCount) {
        // shame modifier!
        if (randomInt(0, 100) < 50) {
            shameModifier = 0.5; // shameless
        } else {
            shameModifier = 2; // shy
        }
    }

    return shameModifier;
}

function logCase(kase: Case) {
    function symptomString(presentationSym: SymptomPresentation) {
        return `${presentationSym.symptom.id} (${presentationSym.symptomState.toString()})`;
    }

    let symptomsString = "";

    for (let symptom of kase.presentation) {
        if (symptom.symptomState !== SymptomState.Absent) {
            symptomsString += "; " + symptomString(symptom);
        }
    }

    console.log(`${kase.condition.id} (${symptomsString})`);
}

async function main() {
    console.log("loading diagnosis occurrences...");

    let occurrences = await loadOccurrences();

    for (let id of occurrences.keys()) {
        console.log(`\t${id}: ${occurrences.get(id)!.occurrenceRate}`);
    }

    console.log("loading diagnoses...");

    const diagnosisFiles = [ "DiagnosesCARDIO.xml", "DiagnosesER.xml",
        "DiagnosesINTERN.xml", "DiagnosesNEURO.xml", "DiagnosesORTHO.xml",
        "DiagnosesSURG.xml" ];
    let allDiagnoses: Diagnoses = new Map();

    for (let file of diagnosisFiles) {
        console.log(`\t${file}`);
        let diagnoses = await loadDiagnoses(file);

        allDiagnoses.merge(diagnoses);
    }

    console.log("loading symptoms...");

    const symptomFiles = [ "Symptoms.xml", "Symptoms_main.xml" ];
    let allSymptoms: Symptoms = new Map();

    for (let file of symptomFiles) {
        console.log(`\t${file}`);
        let symptoms = await loadSymptoms(file);

        allSymptoms.merge(symptoms);
    }

    process.stdout.write("generating cases...\r");

    let cases: Case[] = [];

    for (let diagnosisId of allDiagnoses.keys()) {
        process.stdout.write(`generating cases... (${diagnosisId})\r`);

        let diagnosis = allDiagnoses.get(diagnosisId)!;
        let occurrence = occurrences.get(diagnosis.occurrenceId);

        // TODO: currently this generates 500 cases of "rare" diseases, which
        // have an occurrence rate of 50; this is enough, but if there"s a lower
        // floor to the occurrence rate, we might not get enough samples to have
        // a reasonably full distribution of clinical presentations
        for (let i = 0; i < 10 * occurrence!.occurrenceRate; i++) {
            // generation algorithm here borrows from
            // MedicalCondition.CreateMedicalCondition in the game itself
            let presentation: SymptomPresentation[] = [];
            let anyComplaints = false;

            // first-pass: generate symptoms based on symptom rules
            for (let symptomRule of diagnosis.symptomRules) {
                let symptom = allSymptoms.get(symptomRule.symptomRef)!;
                let ageDays = Math.random() * ((diagnosis.duration / 2) - 1) + 1;
                let symptomState = SymptomState.Absent;

                if (symptom.isMainSymptom || Math.random() > symptomRule.probability) {
                    let spawnTimeDays: number;

                    if (symptom.isMainSymptom || symptomRule.dayOfFirstOccurrence === 0) {
                        // symptom started one hour ago
                        spawnTimeDays = ageDays - 0.4166667;
                    } else {
                        // symptom started/will start somewhere between day n
                        // and the midpoint of the course of the condition
                        spawnTimeDays = Math.random() * 
                            ((diagnosis.duration / 2) - symptomRule.dayOfFirstOccurrence) +
                            symptomRule.dayOfFirstOccurrence;
                    }

                    let symptomShame = symptom.shameLevel * generatePatientShameModifier();
                    let patientKnowsAndComplains =
                        symptom.patientComplains &&
                        (Math.random() > symptomShame) &&
                        (spawnTimeDays < ageDays);

                    if (patientKnowsAndComplains) {
                        symptomState = SymptomState.Complaint;
                        anyComplaints = true;
                    } else if (spawnTimeDays > ageDays) {
                        symptomState = SymptomState.Latent;
                    } else {
                        symptomState = SymptomState.Hidden;
                    }
                }

                presentation.push({
                    symptomRule, symptom, symptomState
                });
            }

            // second pass: if no symptoms are in complaint state, reroll all of
            // them (skipping the first-day-of-occurrence and symptom
            // probability checks) and see if any of them stick this time, then
            // choose one of the new complaints randomly
            //
            // BUG: this will keep rerolling symptoms until it finds one
            // to complain about, which is slightly different behavior from
            // the game (which effectively tries each symptom once, then
            // gives up) but is way less annoying to implement and I"m not sure
            // it"s much different in practice
            if (!anyComplaints) {
                while (true) {
                    let symptomIdx = randomInt(0, presentation.length);
                    let presentationSym = presentation[symptomIdx];

                    let symptomShame = presentationSym.symptom.shameLevel *
                        generatePatientShameModifier();
                    if (presentationSym.symptom.patientComplains &&
                            Math.random() > symptomShame) {
                        presentationSym.symptomState = SymptomState.Complaint;
                        break;
                    }
                }
            }

            let kase: Case = { condition: diagnosis, presentation };

            // logCase(kase);
            cases.push(kase);
        }
    }

    console.log();

    process.stdout.write("saving cases...\r");

    let caseDb = [];

    // okay, now we have some cases, time to write them to a file
    let dbFile = await util.promisify(fs.open)("case-db.json", "w+");
    let i = 0, n = cases.length;

    for (let kase of cases) {
        let caseDbEntry: CaseDbEntry = {
            symptoms: {},
            label: kase.condition.id
        };

        for (let presentationSym of kase.presentation) {
            if (presentationSym.symptomState !== SymptomState.Absent) {
                caseDbEntry.symptoms[presentationSym.symptom.id] =
                    presentationSym.symptomState;
            }
        }

        await util.promisify(fs.write)(dbFile, 
            JSON.stringify(caseDbEntry) + "\n");

        i++;

        if (i % 100 === 0) {
            process.stdout.write(`saving cases... (${i}/${n})\r`);
        }
    }

    console.log();

    await util.promisify(fs.close)(dbFile);
}

main();