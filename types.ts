export type DiagnosisOccurrences = Map<string, { occurrenceRate: number }>;

export type DiagnosisSymptomRule = {
    symptomRef: string;
    dayOfFirstOccurrence: number;
    probability: number;
};

export type Diagnosis = {
    id: string;
    duration: number;
    occurrenceId: string;
    symptomRules: DiagnosisSymptomRule[];
    departmentId: string;
};
export type Diagnoses = Map<string, Diagnosis>;

export type Symptom = {
    id: string;
    patientComplains: boolean;
    examinationIds: string[];
    treatmentIds: string[];
    isMainSymptom: boolean;
    shameLevel: number;
};
export type Symptoms = Map<string, Symptom>;

export enum SymptomState {
    Complaint,
    Hidden,
    Latent,
    Absent
};

export type SymptomPresentation = {
    symptomRule: DiagnosisSymptomRule;
    symptom: Symptom;
    symptomState: SymptomState;
};

export type Case = {
    condition: Diagnosis;
    presentation: SymptomPresentation[];
};

export type CaseDbEntry = {
    symptoms: { [symptomId: string]: SymptomState };
    label: string;
};