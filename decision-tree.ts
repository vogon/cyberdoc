import { CaseDbEntry, SymptomState } from './types';

type DiagnosisNode = {
    diagnosisId: string;
};

type AmbiguousNode = {
    diagnosisId: undefined;
    symptomId: string;
    state: SymptomState;
    ifTrue: Node;
    ifFalse: Node;
}

type Node = DiagnosisNode | AmbiguousNode;

export type Decision = 
    { symptomId: string; symptomState: SymptomState; ptr: AmbiguousNode; } |
    { diagnosisId: string; };

function partitionCases(cases: CaseDbEntry[], partitionSymptomId: string, 
        partitionSymptomState: SymptomState): 
        { inside: CaseDbEntry[], outside: CaseDbEntry[] } {
    let inside: CaseDbEntry[] = [], outside: CaseDbEntry[] = [];

    for (let i = 0; i < cases.length; i++) {
        let kase = cases[i];
        let symptomState = kase.symptoms[partitionSymptomId];

        if (symptomState === partitionSymptomState ||
                (partitionSymptomState === SymptomState.Absent && 
                 symptomState === undefined)) {
            inside.push(kase);
        } else {
            outside.push(kase);
        }
    }

    return { inside, outside };
}

// performance optimization: instead of doing a full entropy calculation, just 
// check whether the amount of entropy is non-zero
function fastZeroEntropy(cases: CaseDbEntry[]): boolean {
    if (cases.length === 0) return true;

    let firstLabel = cases[0].label;

    for (let i = 1; i < cases.length; i++) {
        if (cases[i].label !== firstLabel) return false;
    }

    return true;
}

function classificationEntropy(cases: CaseDbEntry[]): number {
    let labelCounts: { [diagnosisId: string]: number } = {};

    // count up all the labels
    for (let i = 0; i < cases.length; i++) {
        let kase = cases[i];
        let count = labelCounts[kase.label] || 0;
        labelCounts[kase.label] = count + 1;
    }

    let entropy = 0;

    // calculate the entropy for each label and sum
    for (let label in labelCounts) {
        let p = labelCounts[label] / cases.length;
        entropy += -(p * Math.log2(p));
    }

    return entropy;
}

function buildTree(cases: CaseDbEntry[], allSymptoms: string[]): Node {
    let bestSymptom: string | undefined, bestState: SymptomState | undefined;
    let bestEntropy = Infinity;
    let trueCases: CaseDbEntry[] = [], falseCases: CaseDbEntry[] = [];

    if (fastZeroEntropy(cases)) {
        // these cases are unambiguous!  return a leaf (diagnosis) node
        return {
            // all the labels should be identical; choose one arbitrarily
            diagnosisId: cases[0].label
        };
    }

    const allStates = [ SymptomState.Complaint, SymptomState.Hidden,
        SymptomState.Latent, SymptomState.Absent ];

    let startTime = Date.now();

    for (let symptom of allSymptoms) {
        process.stdout.write(
            `buildTree(n = ${cases.length})... ${symptom}?${' '.repeat(50)}\r`
        );

        for (let state of allStates) {
            let { inside, outside } = partitionCases(cases, symptom, state);

            let entropy =
                (classificationEntropy(inside) * inside.length / cases.length) +
                (classificationEntropy(outside) * outside.length / cases.length);

            if (entropy < bestEntropy) {
                // making this choice reduces the entropy lower than our
                // current best; it's our new best
                bestSymptom = symptom;
                bestState = state;
                bestEntropy = entropy;
                trueCases = inside;
                falseCases = outside;
            }
        }
    }

    let endTime = Date.now();

    console.log(`buildTree(n = ${cases.length})... ${bestSymptom}, ${bestState} (${bestEntropy}b; ${endTime-startTime}ms)`);

    // we've chosen an optimal split; recurse to the two case subsets
    let trueNode = buildTree(trueCases, allSymptoms);
    let falseNode = buildTree(falseCases, allSymptoms);

    // and return the root
    return {
        diagnosisId: undefined,
        symptomId: bestSymptom!,
        state: bestState!,
        ifTrue: trueNode,
        ifFalse: falseNode
    }
}

export class DecisionTree {
    private model: AmbiguousNode | undefined;

    constructor(savedModel?: string) {
        if (savedModel !== undefined) {
            this.model = JSON.parse(savedModel);
        } else {
            this.model = undefined;
        }
    }

    train(trainingData: CaseDbEntry[]) {
        // enumerate all of the symptoms in the training set
        let symptomSet = new Set<string>();
        for (let kase of trainingData) {
            for (let symptom in kase.symptoms) {
                symptomSet.add(symptom);
            }
        }

        let allSymptoms: string[] = [];
        for (let symptom of symptomSet.values()) {
            allSymptoms.push(symptom);
        }

        this.model = buildTree(trainingData, allSymptoms) as AmbiguousNode;
    }

    nextDecision(outcome?: boolean, ptr?: AmbiguousNode): Decision | undefined {
        if (ptr) {
            let nextNode = outcome ? ptr.ifTrue : ptr.ifFalse;

            if (nextNode.diagnosisId !== undefined) {
                return { diagnosisId: nextNode.diagnosisId };
            } else {
                return {
                    symptomId: nextNode.symptomId,
                    symptomState: nextNode.state,
                    ptr: nextNode as AmbiguousNode
                };
            }
        } else {
            if (this.model !== undefined) {
                return {
                    symptomId: this.model.symptomId,
                    symptomState: this.model.state,
                    ptr: this.model
                };
            } else return undefined;
        }
    }

    save(): string {
        return JSON.stringify(this.model);
    }
}